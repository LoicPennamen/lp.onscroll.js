/*
This script handles scrolling events
- Allows use of fluid scroll effects on mobile
- Adds a throttle for performance

How to use:
LP.onscroll.register(function(){
	// This script will be executed continuously during scrolling
	console.log('Hi world!');
});

Author: Loïc Pennamen 2017
Requires Jquery -- Can be improved to run without.
*/


// LP is the object carrying all my "Loic Pennamen" methods
if(typeof(LP) == "undefined")
	var LP = {};

// Protected functions
(function ($, root, LP) {
	$(function () {
		'use strict';
		
		/* ------------- */
		/* METHODS       */
		/* ------------- */
		LP.onscroll = {
			'debug' : false,		// Display debug infos
			'throttle' : 30, 		// milliseconds between each actions call
			'failsafe' : 1000,		// If an error occurs during script, a failsafe will stop the stack execution every N milliseconds
			'scrolling' : false,	// Current page status, scrolling or not
			'interval' : null,		// Container for scrolling interval
			'stack' : [],			// Stack of actions to execute on scroll
			'lastPosition' : -1,	// Last known scrolling position
			
			// Register which events trigger scroll-start
			'init': function (){
				
				// Register events that trigger scroll start
				$(document).on('scroll', function(){
					LP.onscroll.start();
				});
				$('body').on('touchstart', function(){
					LP.onscroll.start();
				});
				
			},
			
			// On scroll start
			'start' : function(){
				
				// If not already running
				if(this.scrolling == false){
					
					// log
					if(this.debug)
						console.log('start at ' + (new Date()).getTime());
					
					// Change status
					this.scrolling = true;
					
					// Start timer
					this.interval = setInterval(function(){
						LP.onscroll.executeStack();
						LP.onscroll.checkScrolling();
					}, this.throttle);
					
				}
			},
			
			// Stop scrolling events
			'stop' : function(){
				
				// Stop interval
				clearInterval(this.interval);
				
				// Update status
				this.scrolling = false;
			},
			
			// Execute stack of registered actions
			'executeStack' : function(){
				
				// log
				if(this.debug)
					console.log('executeStack at ' + (new Date()).getTime());
				
				var action = null;
				for (var i = 0; i < this.stack.length; i++) {
					action = this.stack[ i ];
					action();
				}
			},
			
			// Checks whether we are still scrolling, and interrupts stack execution
			'checkScrolling' : function(){
				
				// Last scrolling position:
				if(this.getLastPosition() == this.getPosition())
					this.stop();
				
				// Update last known position
				LP.onscroll.updateLastPosition();
			},
			
			// Get last known scrolling position
			'getLastPosition' : function(){
				return this.lastPosition;
			},
			
			// Get scrolling position
			'getPosition' : function(){
				return $(document).scrollTop();
			},
			
			// Update last known scrolling position
			'updateLastPosition' : function(){
				this.lastPosition = this.getPosition();
			},
			
			// Register on scroll action
			'register' : function(action){
				this.stack[ this.stack.length ] = action;
			}
			
		};
		
		
		
		/* ------------- */
		/* ON PAGE READY */
		/* ------------- */
		$(document).on('ready', function(){
			LP.onscroll.init();
			
			// Uncomment below to test
			/*
			LP.onscroll.register(function(){
				console.log('it works: ' + (new Date()).getTime());
			})
			*/
			
		})
		
	});
})(jQuery, this, LP);
