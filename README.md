# LP Scrolling plugin #

This script handles scrolling events

* Allows use of fluid scroll effects on mobile
* Adds a throttle for performance

It uses a container object "LP" (my own little plugins container), including the onscroll object.

## Dependencies ##
Jquery -- Can be improved to run without.

## How to use: ##

```javascript
LP.onscroll.register(function(){
	// This script will be executed continuously during scrolling
	console.log('Hi world!');
});
```

## Author ##
Lo�c Pennamen - 2017